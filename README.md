Role Name
=========

This role ensures that the Docker Community Edition YUM Repository for CentOS 7 is present and that the docker-ce YUM package is present.

The role also ensures that the docker systemd service unit is enabled and in a started state.

Requirements
------------

Internet connectivity is required, as this role connects to https://download.docker.com from the target host to download the RPM packages.

Role Variables
--------------

This role has no variables.

Dependencies
------------

There are no ansible role dependencies.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Docker Community Edition on CentOS
      hosts: centos
      roles:
        - { role: rit.cen.docker-ce }

License
-------

BSD

Author Information
------------------

Contact http://www.regal-it.com.au/
